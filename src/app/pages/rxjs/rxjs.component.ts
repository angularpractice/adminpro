import { Component, OnInit, OnDestroy } from "@angular/core";
import { Observable } from "rxjs/internal/Observable";
import { Subscriber } from "rxjs/internal/Subscriber";
import { retry, map, filter } from "rxjs/operators";
import { Subscription } from 'rxjs';

@Component({
  selector: "app-rxjs",
  templateUrl: "./rxjs.component.html",
  styles: []
})
export class RxjsComponent implements OnInit, OnDestroy {

  subscibtion: Subscription

  constructor() {
    this.subscibtion = this.regresaObservable().subscribe(
      numero => console.log("Subs", numero),
      error => console.error(error),
      () => console.log("el observador termino")
    );
  }

  ngOnInit() {}

  ngOnDestroy(){
    console.log('la pagina va a cerrar')
    this.subscibtion.unsubscribe();
  }

  regresaObservable(): Observable<any> {
    return new Observable((observer: Subscriber<any>) => {
      let contador = 0;
      let intervalo = setInterval(() => {
        contador++;

        const salida = {
          valor: contador
        };

        observer.next(salida);
      }, 1000);
    }).pipe(
      map(resp => resp.valor),
      filter((valor, index) => {
        if( (valor % 2) === 1){
          // impar
          return false
        }
        // par
        return true;
      })
    );
  }
}
